#!/usr/bin/env python

"""
    PyChronopost

    :copyright: (c) 2020 Quentin THEURET
    :copyright: (c) 2020 Voie Lactée
    :licence: APGLv3, see LICENSE for more details
"""

import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

with open('pychronopost/version.py') as f:
    # execute the file so __version__ is in the current scope
    exec(f.read())

setup(
    # Basic package information
    name = 'pychronopost',
    version = __version__,

    # Packaging options.
    include_package_date = True,

    # Package dependencies.
    install_requires = [
        'zeep',
    ],

    # Metadata for PyPI.
    author = 'Quentin THEURET',
    author_email = 'contact@kerpeo.com',
    license = 'GNU APGL-v3',
    url = 'http://github.com/qtheuret/pychronopost',
    packages=['pychronopost'],
    keywords = 'chronopost api client soap',
    description = 'A library to access Chronopost Web Service from Python.',
    long_description = read('README.md'),
    classifiers = [
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero Genera Public License v3',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Internet :: WWW/HTTP :: Site Management',
        'Topic :: Internet',
    ]
)