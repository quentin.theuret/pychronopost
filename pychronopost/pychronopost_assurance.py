

class PyChronopostAssurance(object):
    """

    """

    def __init__(self, plafond, taux):
        """
        Initialization of the assurance
        :param plafond: Max amount of the insured products
        :param taux: Insurance rate
        """
        self.plafond = plafond
        self.taux = taux