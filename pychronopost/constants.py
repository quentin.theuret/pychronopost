# -*- coding: utf-8 -*-
""" Set of Chronopost WebServices constants

This module contains constants that are described in the Chronopost WebServices documentation.

:author: `Quentin THEURET <contact@kerpeo.com>` and others
:organisation: Kerpeo and Grandir Nature
:copyright: Copyright (c) 2020 Kerpeo and Grandir Nature
:license: MIT
"""

WS_URL = 'https://ws.chronopost.fr'

# National products
CHRONO13_CODE = 1
CHRONO10_CODE = 2
CHRONO18_CODE = 16
CHRONORELAIS_CODE = 86
CHRONORETRAIT_BUREAU_CODE = 00

# International products
CHRONOEXPRESS_CODE = 17
CHRONOPREMIUM_CODE = 37
CHRONOCLASSIC_CODE = 44

PRODUCTS = [
    CHRONO10_CODE,
    CHRONO13_CODE,
    CHRONO18_CODE,
    CHRONOCLASSIC_CODE,
    CHRONOEXPRESS_CODE,
    CHRONOPREMIUM_CODE,
    CHRONORELAIS_CODE,
    CHRONORETRAIT_BUREAU_CODE,
]

# Service codes
ENLEVEMENT_CODE = 1
RETOUR_EXPRESS_CODE = 4
LIV_SAMEDI_CODE = 12
LIV_SAMEDI_EXP_CODE = 15
LIV_DOMI_EXP_CODE = 'B1'
LIV_DOMI_CODE = 'B2'
