#!/usr/bin/python
# -*- coding: utf-8 -*-
""" Main class

This module contains the abstract base class :py:class:`ChronopostWS`.

:author: `Quentin THEURET <contact@kerpeo.com>`_ and others
:organisation: Kerpeo and Grandir Nature
:copyright: Copyright (c) 2020 Kerpeo and Grandir Nature
:license: MIT
"""
from zeep import Client
from zeep.wsse.username import UsernameToken
from zeep.plugins import HistoryPlugin

from .constants import WS_URL, PRODUCTS

from .pychronopost_assurance import PyChronopostAssurance
from .pychronopost_service import PyChronopostService


class ChronopostWSRequest(object):
    """ ChronopostWS object

    This class is the abstract base class for any Chronopost WebServices. The WebServices implementations are
    children of this class.
    """
    service_url = False     # URL of the service
    method_name = False     # Name of the method to call
    auth = False            # Need an authentication


class PyChronopostQuickCost(ChronopostWSRequest):
    """

    """
    service_url = '%s/quickcost-cxf/QuickcostServiceWS' % WS_URL
    service_wsdl_url = '%s?wsdl' % service_url
    method_name = 'quickCost'

    def __init__(self, account_number, depcode, arrcode, weight, product_code, password, product_type):
        """
        Compute the price of each available products for a shipment.
        :param account_number: The account number of the Chronopost account
        :param depcode: Zip code of departure
        :param arrcode: Zip code of arrival
        :param weight:  Weight of the shipment
        :param product_code: Chronopost Product Code (see constants.py)
        :param password: Password of the Chronopost account
        :param product_type: Document ("D") or material ("M")
        :return: ChronopostRequest
        """
        if product_code not in PRODUCTS:
            raise Exception(
                'ProducCode %s is not a valid product code (Should be in %s)' % (
                    product_code,
                    [', '.join(x for x in PRODUCTS)],
                )
            )

        if product_type not in ["D", "M"]:
            raise Exception(
                'Type %s is not a valid product (Should be "M" or "D").' % product_type
            )

        # Request fields
        self.account_number = account_number
        self.depcode = depcode
        self.arrcode = arrcode
        self.weight = weight
        self.product_code = product_code
        self.password = password
        self.type = product_type

        # Respons fields
        self.zone = None
        self.amount = None
        self.amount_ttc = None
        self.amount_tva = None
        self.services = []
        self.assurance = None


    def quickCost(self):
        """
        Run the request
        :return:
        """
        history = HistoryPlugin()
        client = Client(self.service_wsdl_url, plugins=[history])
        result = client.service.quickCost(
            accountNumber=self.account_number,
            depCode=self.depcode,
            arrCode=self.arrcode,
            weight=self.weight,
            productCode=self.product_code,
            password=self.password,
            type=self.type,
        )

        if result['errorCode']:
            raise Exception(
                '%s - %s' % (
                    result['errorCode'],
                    result['errorMessage'],
                )
            )
        else:
            services = []
            for s in result['service']:
                service = PyChronopostService(
                    s['codeService'],
                    s['label'],
                    s['amount'],
                    s['amountTTC'],
                    s['amountTVA'],
                )
                services.append(service)

            self.zone = result['zone']
            self.amount = result['amount']
            self.amount_ttc = result['amountTTC']
            self.amount_tva = result['amountTVA']
            self.services = services
            self.assurance = PyChronopostAssurance(
                result['assurance']['plafond'],
                result['assurance']['taux'],
            )


class PyChronopostCalculateProducts(ChronopostWSRequest):
    """

    """
    service_url = '%s/quickcost-cxf/QuickcostServiceWS' % WS_URL
    service_wsdl_url = '%s?wsdl' % service_url
    method_name = 'quickCost'

    def __init__(self, account_number, depcode, arrcode, weight, product_code, password, product_type):
        """
        Compute the price of each available products for a shipment.
        :param account_number: The account number of the Chronopost account
        :param depcode: Zip code of departure
        :param arrcode: Zip code of arrival
        :param weight:  Weight of the shipment
        :param product_code: Chronopost Product Code (see constants.py)
        :param password: Password of the Chronopost account
        :param product_type: Document ("D") or material ("M")
        :return: ChronopostRequest
        """
        if product_code not in PRODUCTS:
            raise Exception(
                'ProducCode %s is not a valid product code (Should be in %s)' % (
                    product_code,
                    [', '.join(x for x in PRODUCTS)],
                )
            )

        if product_type not in ["D", "M"]:
            raise Exception(
                'Type %s is not a valid product (Should be "M" or "D").' % product_type
            )

        # Request fields
        self.account_number = account_number
        self.depcode = depcode
        self.arrcode = arrcode
        self.weight = weight
        self.product_code = product_code
        self.password = password
        self.type = product_type

        # Respons fields
        self.zone = None
        self.amount = None
        self.amount_ttc = None
        self.amount_tva = None
        self.services = []
        self.assurance = None


    def quickCost(self):
        """
        Run the request
        :return:
        """
        history = HistoryPlugin()
        client = Client(self.service_wsdl_url, plugins=[history])
        result = client.service.quickCost(
            accountNumber=self.account_number,
            depCode=self.depcode,
            arrCode=self.arrcode,
            weight=self.weight,
            productCode=self.product_code,
            password=self.password,
            type=self.type,
        )

        if result['errorCode']:
            raise Exception(
                '%s - %s' % (
                    result['errorCode'],
                    result['errorMessage'],
                )
            )
        else:
            services = []
            for s in result['service']:
                service = PyChronopostService(
                    s['codeService'],
                    s['label'],
                    s['amount'],
                    s['amountTTC'],
                    s['amountTVA'],
                )
                services.append(service)

            self.zone = result['zone']
            self.amount = result['amount']
            self.amount_ttc = result['amountTTC']
            self.amount_tva = result['amountTVA']
            self.services = services
            self.assurance = PyChronopostAssurance(
                result['assurance']['plafond'],
                result['assurance']['taux'],
            )