

class PyChronopostService(object):
    """
    Define a Chronopost sevice
    """
    def __init__(self, code, label, amount, amount_ttc, amount_tva):
        """

        :param code: Product Code
        :param label: Label of the product
        :param amount: Cost without tax of the service
        :param amount_ttc: Cost with tax of the service
        :param amount_tva: Tax amount of the service
        """
        self.code = code
        self.label = label
        self.amount = amount
        self.amount_ttc = amount_ttc
        self.amount_tva = amount_tva
