# PyChronopost

pychronopost is a library for Python to interact with the Chronopost's Web Service API.

Request the PDF of the Chronopost' Web Services Documentation to your account manager.

## Installation

The easiest way to install pychronopost (needs setuptools):

    easy_install pychronopost

Or, better, using pip:

    pip install pychronopost

If you do not have setuptools, download pychronopost as a .tar.gz or .zip from
[PyChronopost Source Archives], untar it and run:

    python setup.py install

In order to always be uptodate, the best way is to use pip from this repo with the following command :

    pip install --ignore-installed git+https://github.com/prestapyt/prestapyt.git@master

## Usage


## Copyright and License

pychronopost is copyright (c) 2020 Voie Lactée

pychronopost is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

pychronopost is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with prestapyt. If not, see [GNU licenses](http://www.gnu.org/licenses/).



[PyChronopost Source Archives]: https://github.com/guewen/prestapyt/downloads
